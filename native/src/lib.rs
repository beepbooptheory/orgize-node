use neon::prelude::*;
use orgize::Org;

fn hello(mut cx: FunctionContext) -> JsResult<JsString> {
    Ok(cx.string("hello node"))
}

fn helllo(mut cx: FunctionContext) -> JsResult<JsString> {
    Ok(cx.string("hello other node"))
}

fn org_parse(mut cx: FunctionContext) -> JsResult<JsString> {
    let mut writer = Vec::new();
    let arg0 = cx.argument::<JsString>(0)?.value();

    // cx.check_argument::<JsString>(0)?;
    Org::parse(&String::from(arg0)).write_html(&mut writer).unwrap();
    // String::from_utf8(writer).unwrap();
    Ok(cx.string(String::from_utf8(writer).unwrap()))
}
register_module!(mut cx, {
    cx.export_function("hello", hello)?;
    cx.export_function("helllo", helllo)?;
    cx.export_function("orgParse", org_parse)?;
    Ok(())
});
